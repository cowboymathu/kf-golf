defmodule Calculator do
  @input_map %{
    "zero" => "0",
    "one" => "1",
    "two" => "2",
    "three" => "3",
    "four" => "4",
    "five" => "5",
    "six" => "6",
    "seven" => "7",
    "eight" => "8",
    "nine" => "9",
    "ten" => "10",
    "plus" => "+",
    "minus" => "-",
    "times" => "*"
  }

  def run(input) do
    case String.valid?(input) do
      false ->
        "Invalid input"

      true ->
        case String.replace(input, [" " | Map.keys(@input_map)], "") do
          "" ->
            input =
              input
              |> String.replace(Map.keys(@input_map), fn x -> Map.get(@input_map, x) end)
              |> String.replace(" ", "")

            parse_input(input)

          _ ->
            "Invalid input"
        end
    end
  end

  defp parse_input(input) do
    regex =
      ~r/^(?<number1>[-|\+]*\d+)(?<operator1>(\+|-|\*))(?<number2>[-|\+]*\d+)(?<operator2>(\+|-|\*))(?<number3>[-|\+]*\d+)(?<rest>.*)/

    case Regex.named_captures(regex, input) do
      %{
        "number1" => n1,
        "operator1" => o1,
        "number2" => n2,
        "operator2" => o2,
        "number3" => n3,
        "rest" => rest
      } ->
        fn1 = format_number(n1)
        fn2 = format_number(n2)
        fn3 = format_number(n3)

        case o2 do
          "*" ->
            input = "#{n1}#{o1}#{fn2 * fn3}#{rest}"
            parse_input(input)

          _ ->
            input =
              case o1 do
                "*" ->
                  "#{fn1 * fn2}#{o2}#{n3}#{rest}"

                "+" ->
                  "#{fn1 + fn2}#{o2}#{n3}#{rest}"

                "-" ->
                  "#{fn1 - fn2}#{o2}#{n3}#{rest}"
              end

            parse_input(input)
        end

      nil ->
        regex = ~r/^(?<number1>[-|\+]?\d+)(?<operator1>(\+|-|\*))(?<number2>[-|+]?\d+)/

        case Regex.named_captures(regex, input) do
          %{"number1" => n1, "operator1" => o1, "number2" => n2} ->
            fn1 = format_number(n1)
            fn2 = format_number(n2)

            input =
              case o1 do
                "*" ->
                  "#{fn1 * fn2}"

                "+" ->
                  "#{fn1 + fn2}"

                "-" ->
                  "#{fn1 - fn2}"
              end

            parse_input(input)

          nil ->
            try do
              String.to_integer(input)
            rescue
              ArgumentError -> "Invalid input"
            end
        end
    end
  end

  defp format_number(number) do
    number
    |> String.replace("+", "")
    |> String.replace("--", "")
    |> String.to_integer()
  end
end
