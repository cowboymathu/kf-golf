defmodule Calculator2 do
  def run(args) do
    args
      |> String.split(" ")
      |> Enum.reject(fn x -> x == "" end)
      |> Enum.map(fn x -> i(x) end)
      |> i()
  end
  defp i(["plus" | r]), do: i(r)
  defp i(["minus" | r]), do: - i(r)
  defp i([n | ["times" | ["plus" | r]]]), do: i([n | ["times" | r]])
  defp i([n | ["times" | ["minus" | r]]]), do: - i([n | ["times" | r]])
  defp i([n | ["times" | [m | r]]]), do: i([n * m | r])
  defp i([n | ["plus" | r]]), do: i(n) + i(r)
  defp i([n | ["minus" | r]]), do: i(n) - i(r)
  defp i([n]), do: i(n)
  defp i("zero"), do: 0
  defp i("one"), do: 1
  defp i("two"), do: 2
  defp i("three"), do: 3
  defp i("four"), do: 4
  defp i("five"), do: 5
  defp i("six"), do: 6
  defp i("seven"), do: 7
  defp i("eight"), do: 8
  defp i("nine"), do: 9
  defp i("ten"), do: 10
  defp i(i), do: i
end
